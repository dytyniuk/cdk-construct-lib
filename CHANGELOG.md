
### [0.1.13](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.12...v0.1.13) (2022-10-05)

### [0.1.12](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.11...v0.1.12) (2022-10-05)

### [0.1.11](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.10...v0.1.11) (2022-10-05)

### [0.1.10](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.9...v0.1.10) (2022-10-05)

### [0.1.9](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.8...v0.1.9) (2022-10-05)

### [0.1.8](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.7...v0.1.8) (2022-10-05)

### [0.1.7](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.6...v0.1.7) (2022-10-05)

### [0.1.6](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.5...v0.1.6) (2022-10-05)

### [0.1.5](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.4...v0.1.5) (2022-10-05)


### Bug Fixes

* **ci:** install rsync for projen release task ([90177f9](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/90177f9950f89b3ea2bb92d82ef32293c62c3534))

### [0.1.4](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.3...v0.1.4) (2022-10-04)

### [0.1.3](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.2...v0.1.3) (2022-10-04)

### [0.1.2](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.1...v0.1.2) (2022-10-04)

### [0.1.1](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.1.0...v0.1.1) (2022-09-30)


### Features

* say hi ([0fd1676](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/0fd1676c1a992ff5629160e24fd198504d6747b9))

## [0.1.0](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.9...v0.1.0) (2022-09-29)


### ⚠ BREAKING CHANGES

* `sayHi` and other informal methods are removed

### Features

* no more informal greetings ([3af868b](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/3af868b680684e1cbcc2aa7d43e4b5fec793316a))

### [0.0.10](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.9...v0.0.10) (2022-09-29)


### Features

* something very important ([0b59f07](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/0b59f07b6242f338947b2761bc8b94c82ae4d210))

### [0.0.9](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.8...v0.0.9) (2022-09-26)


### Features

* say good afternoon ([8171ffc](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/8171ffc5b583e4e30ed0166e038f20921158f6d5))

### [0.0.9](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.8...v0.0.9) (2022-09-26)


### Features

* say good afternoon ([8171ffc](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/8171ffc5b583e4e30ed0166e038f20921158f6d5))

### [0.0.8](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.7...v0.0.8) (2022-09-26)


### Features

* say good evening ([c929042](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/c9290427a09c733058eddce0a28560c8f02daab1))
* say good evening ([38dd02d](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/38dd02d96f5fa6da2861922ca16945c0c6fa45f0))

### [0.0.7](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.6...v0.0.7) (2022-09-23)


### Features

* You can say "Hi!" ([35f3a5e](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/35f3a5e647a852a15426e102701f8534ed827b57))

### [0.0.6](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.5...v0.0.6) (2022-09-21)


### Features

* great in the morning ([841b565](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/841b56533f2df6054ba9748f9f574241df12cafb))

### [0.0.5](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.4...v0.0.5) (2022-09-20)


### Features

* add spanish greating ([8b3e5ae](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/8b3e5aed0a6125ff9141d2fc94669f4207c008a9))

### [0.0.4](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.3...v0.0.4) (2022-09-20)


### Features

* dutch 'hi' ([b620dd9](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/b620dd9cd66aacd9ea0df871c454434272dab37e))

### [0.0.3](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.2...v0.0.3) (2022-09-19)


### Features

* say howdy ([ee88a0a](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/ee88a0a547b9fd9e0e62a392bec08b919a83487b))

### [0.0.2](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.1...v0.0.2) (2022-09-19)


### Features

* capitalize ([47f667d](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/47f667d8b991f7002af71baeadc494fd12300501))

### [0.0.1](https://gitlab.com/dytyniuk/cdk-construct-lib/compare/v0.0.0...v0.0.1) (2022-09-19)


### Features

* great my name ([e6b2f90](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/e6b2f900c7b5a3da08e47c5bc2b2fecd5f1f9550))

## 0.0.0 (2022-09-19)


### Features

* first release ([b1d47aa](https://gitlab.com/dytyniuk/cdk-construct-lib/commit/b1d47aa22a0e12c94ced9fcb82ec11c1a1f9cc29))

