export class Hello {
  public sayHello(name: string) {
    return `Hello, ${name}!`;
  }

  public sayGoodMorning(name: string) {
    return `Good morning, ${name}!`;
  }

  public sayGoodEvening(name: string) {
    return `Good evening, ${name}!`;
  }

  public sayGoodAfternoon(name: string) {
    return `Good afternoon, ${name}!`;
  }

  public sayHi(name: string) {
    return `Hi, ${name}!`;
  }
}