const { awscdk } = require('projen');
const { NodePackageManager } = require('projen/lib/javascript');
const { ReleaseTrigger } = require('projen/lib/release');

const project = new awscdk.AwsCdkConstructLibrary({
  author: 'Yevhenii Dytyniuk',
  authorAddress: 'yevhenii.dytyniuk@dustin.com',
  name: 'CDK Construct concept',
  description: 'A concept of a reusable CDK Construct library',
  packageName: '@dytyni.uk/playground-cdk-construct-lib',
  repositoryUrl: 'https://gitlab.com/dytyniuk/cdk-construct-lib',
  keywords: ['cdk', 'poc', 'playground'],
  cdkVersion: '2.1.0',
  defaultReleaseBranch: 'main',
  buildWorkflow: false,
  release: true,
  releaseTrigger: ReleaseTrigger.manual(),
  releaseToNpm: true,
  publishTasks: true,
  pullRequestTemplate: false,
  dependabot: false,
  catalog: { announce: false },
  docgen: false,
  github: false,
  packageManager: NodePackageManager.NPM,
  npmRegistryUrl: 'https://gitlab.com/api/v4/projects/39527323/packages/npm/',
  deps: [],
  peerDeps: [],
  stability: 'experimental',
});

project.synth();