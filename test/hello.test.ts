import { Hello } from '../src';

const h = new Hello();
test('hello', () => {
  expect(h.sayHello('Yev'))
    .toBe('Hello, Yev!');
});

test('good morning', () => {
  expect(h.sayGoodMorning('Tim'))
    .toBe('Good morning, Tim!');
});

test('good evening', () => {
  expect(h.sayGoodEvening('Joe'))
    .toBe('Good evening, Joe!');
});

test('good afternoon', () => {
  expect(h.sayGoodAfternoon('Helen'))
    .toBe('Good afternoon, Helen!');
});

test('hi', () => {
  expect(h.sayHi('Patrick'))
    .toBe('Hi, Patrick!');
});

